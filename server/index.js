import express from 'express';
import consola from 'consola';
import cookieParser from 'cookie-parser';
import { Nuxt, Builder } from 'nuxt';

const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3000;

let configNuxt = require('../nuxt.config.js');
configNuxt.dev = !(process.env.NODE_ENV === 'production');

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(configNuxt);

  // Build only in dev mode
  if (configNuxt.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  }
  app.use(cookieParser());
  app.use(nuxt.render);

  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  });
}
start();

export default {
  app,
  close: () => app.close()
};
