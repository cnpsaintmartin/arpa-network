import locale from 'element-ui/lib/locale/lang/en';

const lang = {
  ...locale,
  language: {
    name: 'English',
    short: 'gb'
  },
  general: {
    yes: 'Yes',
    true: 'Yes',
    no: 'No',
    false: 'No',
    cancel: 'Cancel',
    send: 'Send',
    question: 'Question',
    platformPresentation:
      'ARPA-ageing is the first European platform specialized in geriatric psychiatry'
  },
  statistics: {
    charts: {
      members: {
        users: 'Users',
        professionals: 'Professionals',
        committeeMembers: 'Committee'
      },
      resources: {
        articles: 'Articles',
        events: 'Events',
        surveys: 'Surveys'
      },
      recommendations: {
        recommend: 'Recommends',
        dontRecommend: `Don't Recommends`
      }
    }
  },
  navBar: {
    home: 'Home',
    about: 'About',
    agenda: 'Agenda',
    partners: 'Partners',
    committee: 'Committee',
    publish: 'Publish',
    contact: 'Contact',
    yourOpinion: 'Your opinion !!',
    signUp: 'Sign up',
    signIn: 'Sign in',
    logout: 'Logout',
    administration: 'Administration',
    profile: 'My profile',
    statistics: 'Statistics'
  },
  categories: {
    legislative_and_laws: 'Legislative & laws',
    socio_demographic_data: 'Socio-demographic data',
    organizations: 'Organizations',
    good_practices: 'Good practices',
    bibliography: 'Bibliography',
    training: 'Training',
    news: 'News'
  },
  feedbackForm: {
    title: 'Give us your feedback!',
    text:
      'The ARPA - Ageing platform is a tool for professionals and users of health services, as well as anyone interested in issues related to the support of elderly people with mental health problems.\n' +
      ' \n' +
      'This platform is intended to be participatory, which is why your opinion counts.\n' +
      ' \n' +
      'The questionnaire will not take you more than 3 minutes.\n' +
      ' \n' +
      'Thank you for your participation. \n',
    chooseLanguage: 'Choose your language',
    q1: {
      label:
        'How would you rate your overall experience using the platform (navigation, understanding, etc.)?'
    },
    q2: {
      label: 'Did you find what you were looking for?'
    },
    q3: {
      label: 'Would you recommend this platform to colleagues or relatives?'
    },
    q4: {
      label: 'Does the platform seem exhaustive to you?'
    },
    q5: {
      placeholder: 'Your comment...',
      label: 'What proposals would you make to further improve this platform?'
    }
  }
};

export { lang };
