import locale from 'element-ui/lib/locale/lang/fr';

const lang = {
  ...locale,
  language: {
    name: 'French',
    short: 'fr'
  },
  general: {
    yes: 'Oui',
    no: 'Non',
    cancel: 'Annuler',
    send: 'Envoyer',
    platformPresentation:
      'ARPA-ageing est la première plateforme européenne spécialisée en psychiatrie gériatrique'
  },
  navBar: {
    home: 'Accueil',
    about: 'À propos',
    agenda: 'Agenda',
    partners: 'Partenaires',
    contact: 'Contact',
    committee: 'Comité',
    publish: 'Publier',
    yourOpinion: 'Votre opinion !!',
    signUp: 'Inscription',
    signIn: 'Connexion',
    logout: 'Déconnexion',
    administration: 'Administration',
    profile: 'Mon profil',
    statistics: 'Statistiques'
  },
  categories: {
    legislative_and_laws: 'Legislation et lois',
    socio_demographic_data: 'Données socio-démographique',
    organizations: 'Organisations',
    good_practices: 'Bonnes pratiques',
    bibliography: 'Bibliographies',
    training: 'Formation',
    news: 'Actualités'
  },
  feedbackForm: {
    title: 'Donnez-nous votre avis',
    text:
      'La plateforme ARPA – Ageing est un outil destiné aux professionnels et aux usagers des services de santé, ainsi que toute personne intéressée par les questions de l’accompagnement des personnes âgées ayant un problème de  santé mentale.\n' +
      ' \n' +
      'Cette plateforme se veut participative, c’est pourquoi votre opinion compte.\n' +
      ' \n' +
      'Le questionnaire ne vous prendra pas plus de 3 minutes.\n' +
      ' \n' +
      'Merci de votre participation.\n',
    chooseLanguage: 'Choisissez votre langue',
    q1: {
      label:
        'Comment qualifieriez-vous votre expérience globale d’utilisation de la plateforme (navigation, compréhension, etc.) ?'
    },
    q2: {
      label: 'Avez-vous trouvé ce que vous cherchiez ?'
    },
    q3: {
      label:
        'Conseilleriez-vous cette plateforme à des collègues ou à des connaissances ?'
    },
    q4: {
      label: 'La plateforme vous semble-t-elle exhaustive ?'
    },
    q5: {
      placeholder: 'Votre commentaire...',
      label:
        'Quelles propositions feriez-vous pour continuer à améliorer cette plateforme ?'
    }
  }
};

export { lang };
