import locale from 'element-ui/lib/locale/lang/de';

const lang = {
  ...locale,
  language: {
    name: 'German',
    short: 'de'
  },
  general: {
    yes: 'Ja',
    no: 'Nein',
    cancel: 'Stornieren',
    send: 'Senden',
    platformPresentation:
      'ARPA-aging ist die erste europäische Plattform, die sich auf geriatrische Psychiatrie spezialisiert hat'
  },
  navBar: {
    home: 'Willkommen',
    about: 'Über',
    agenda: 'Tagebuch',
    partners: 'Partner',
    committee: 'Ausschuss',
    publish: 'Veröffentlichen',
    yourOpinion: 'Ihre Meinung !!',
    signUp: 'Anmeldung',
    signIn: 'Einloggen',
    logout: 'Austragen',
    administration: 'Verwaltung',
    profile: 'Mein Profil',
    statistics: 'Statistiken',
    contact: 'Kontakt'
  },
  categories: {
    legislative_and_laws: 'Gesetzgebung und Gesetze',
    socio_demographic_data: 'Soziodemografische Daten',
    organizations: 'Organisationen',
    good_practices: 'Gute Praktiken',
    bibliography: 'Bibliographien',
    training: 'Ausbildung',
    news: 'Nachrichten'
  },
  feedbackForm: {
    title: 'Sagen Sie uns Ihre Meinung',
    text:
      'Die ARPA - Ageing-Plattform ist ein Tool für Fachleute und Nutzer von Gesundheitsdiensten sowie für alle, die sich für Fragen bezüglich der Unterstützung älterer Menschen mit psychischen Problemen interessieren.\n' +
      '\n' +
      'Dies ist eine partizipative Plattform, Ihre Meinung zählt!\n' +
      '\n' +
      'Der Fragebogen dauert nicht länger als 3 Minuten.\n' +
      '\n' +
      'Vielen Dank für Ihre Teilnahme.\n',
    chooseLanguage: 'Wähle deine Sprache',
    q1: {
      label:
        'Wie beurteilen Sie Ihre Gesamterfahrung mit der Nutzung der Plattform (Navigation, Verständnis, etc.)?'
    },
    q2: {
      label: 'Haben Sie gefunden, was Sie gesucht haben?'
    },
    q3: {
      label:
        'Würden Sie diese Plattform an Kollegen oder Bekannte weiterempfehlen?'
    },
    q4: {
      label: 'Erscheint Ihnen die Plattform umfassend?'
    },
    q5: {
      placeholder: 'Dein Kommentar...',
      label:
        'Welche Vorschläge würden Sie unterbreiten, um diese Plattform weiter zu verbessern?'
    }
  }
};

export { lang };
