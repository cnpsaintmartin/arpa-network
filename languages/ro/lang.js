import locale from 'element-ui/lib/locale/lang/ro';

const lang = {
  ...locale,
  language: {
    name: 'Romanian',
    short: 'ro'
  },
  general: {
    yes: 'Da',
    no: 'Nu',
    cancel: 'Anula',
    send: 'Trimite',
    platformPresentation:
      'ARPA-îmbătrânirea este prima platformă europeană specializată în psihiatrie geriatrică'
  },
  navBar: {
    home: 'Bun venit',
    about: 'Despre',
    contact: 'Contact',
    agenda: 'Jurnal',
    partners: 'Parteneri',
    committee: 'Comisie',
    publish: 'Publica',
    yourOpinion: 'Opinia ta !!',
    signUp: 'înregistrare',
    signIn: 'Autentificare',
    logout: 'Deconectați-vă',
    administration: 'Administrare',
    profile: 'Profilul meu',
    statistics: 'Statistică'
  },
  categories: {
    legislative_and_laws: 'Legislație și legi',
    socio_demographic_data: 'Datele socio-demografice',
    organizations: 'Organizații',
    good_practices: 'Bune practici',
    bibliography: 'Bibliografii',
    training: 'Pregătire',
    news: 'știri'
  },
  feedbackForm: {
    title: 'Dați-ne părerea dumneavoastră!',
    text:
      'Platforma ARPA - Ageing este un instrument pentru profesioniștii și utilizatorii serviciilor de sănătate, precum și pentru oricine este interesat de problemele îngrijirii persoanelor în vârstă care suferă de o problemă de sănătate mintală.\n' +
      '\n' +
      'Această platformă se dorește a fi participativă, de aceea opinia dvs. contează.\n' +
      '\n' +
      'Chestionarul nu vă ia mai mult de 3 minute.\n' +
      '\n' +
      'Mulțumim pentru participare.',
    chooseLanguage: 'Alegeți limba dvs',
    q1: {
      label:
        'Cum ați descrie experiența dvs. globală utilizând platforma (navigare, înțelegere, etc.)?'
    },
    q2: {
      label: 'Ați găsit ceea ce căutați?'
    },
    q3: {
      label: 'Ați recomanda această platformă colegilor sau cunoștințelor dvs.?'
    },
    q4: {
      label: 'Platforma pare a fi completă?'
    },
    q5: {
      placeholder: 'Comentariul tău...',
      label: 'Ce propuneți pentru îmbunătățirea platformei?'
    }
  }
};

export { lang };
