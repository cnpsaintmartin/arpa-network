const pkg = require('./package');
const consola = require('consola');

if (process.env.GTAG_ID)
  consola.info('google analytics ID: ', process.env.GTAG_ID);

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src:
          'https://cdn.polyfill.io/v2/polyfill.min.js?features=default,Array.prototype.find,Array.prototype.findIndex'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: ['element-ui/lib/theme-chalk/index.css', '~/css/main.scss'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vue-i18n',
    '@/plugins/element-ui',
    '@/plugins/vue-tour',
    '@/plugins/lodash',
    '@/plugins/moment',
    '@/plugins/lazyLoad',
    '@/plugins/globals',
    '@/plugins/form',
    '@/plugins/notifications',
    '@/plugins/fontAwesome',
    '@/plugins/roles',
    '@/plugins/vue-scrollto',
    '@/plugins/el-search-table-pagination',
    { src: '@/plugins/vue-croppa', ssr: false },
    // '@/plugins/breakpoints',
    { src: '@/plugins/init-store', ssr: false },
    { src: '@/plugins/vue-social-sharing', ssr: false },
    { src: '@/plugins/apexcharts', ssr: false },
    { src: '@/plugins/vue-masonry-css', ssr: false },
    { src: '@/plugins/vue-fullcalendar', ssr: false },
    { src: '@/plugins/vue-json-edit', ssr: false },
    { src: '@/plugins/vue-json-pretty', ssr: false },
    { src: '@/plugins/vue-editor', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/localforage',
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'default',
        breakpoints: {
          xs: 960,
          md: 1250,
          lg: Infinity
        }
      }
    ],
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-133012554-1'
      }
    ]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    prefix: '/api',
    baseURL: process.env.API_HOST || 'http://0.0.0.0:3030',
    proxy: true
  },
  proxy: {
    '/api': {
      target: process.env.API_HOST || 'http://0.0.0.0:3030'
      // pathRewrite: { "^/api": "" }
    }
  },
  env: {
    WS_URL: process.env.WS_URL || 'http://localhost:3030'
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: ['nuxt-vuex-localstorage'],
    vendor: ['babel-es6-polyfill'],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  },
  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      home: '/',
      callback: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/user/authentificate',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          user: { url: '/user', method: 'get', propertyName: 'docs' }
        }
      }
    }
  }
};
