import Vue from 'vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faThumbsUp,
  faExclamationTriangle,
  faBars,
  faTimes,
  faCommentAlt,
  faPlus,
  faThumbtack,
  faClock,
  faTags,
  faEllipsisH,
  faUserFriends,
  faUserAltSlash,
  faUserCheck,
  faNewspaper,
  faCheck,
  faDotCircle,
  faFlag,
  faUsers,
  faComments,
  faBell,
  faSave,
  faUser,
  faAddressCard,
  faTrashAlt,
  faEye,
  faCommentDots
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

export default () => {
  library.add(
    faThumbsUp,
    faExclamationTriangle,
    faBars,
    faTimes,
    faCommentAlt,
    faPlus,
    faThumbtack,
    faClock,
    faTags,
    faEllipsisH,
    faUserFriends,
    faUserAltSlash,
    faUserCheck,
    faNewspaper,
    faCheck,
    faDotCircle,
    faFlag,
    faUsers,
    faComments,
    faBell,
    faSave,
    faUser,
    faAddressCard,
    faTrashAlt,
    faEye,
    faCommentDots
  );
  Vue.component('font-awesome-icon', FontAwesomeIcon);
};
