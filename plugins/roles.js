import Vue from 'vue';
import _ from 'lodash';
export default ({ store }) => {
  const Roles = {
    install(Vue) {
      Vue.prototype.$roles = {
        check(payload, hasRoles) {
          if (!Array.isArray(payload)) return true;
          const userRoles = _.get(store, 'state.auth.user.roles');
          let roles = hasRoles ? hasRoles : userRoles;
          if (!roles) return false;
          roles = roles.map(r => {
            return r.slug;
          });
          return payload.some(e => roles.includes(e));
        }
      };
    }
  };
  Vue.use(Roles, {});
};
