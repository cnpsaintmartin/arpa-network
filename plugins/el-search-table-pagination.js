import Vue from 'vue';
import ElSearchTablePagination from 'el-search-table-pagination';

Vue.use(ElSearchTablePagination);
