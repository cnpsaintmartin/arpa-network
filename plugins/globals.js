import Vue from 'vue';

export default () => {
  Vue.mixin({
    transition: 'fade-fast',
    computed: {
      loggedIn() {
        return this._.get(this.$store, 'state.auth.loggedIn');
      },
      user() {
        return this._.get(this.$store, 'state.auth.user');
      }
    }
  });
  Vue.component('country-flag', () => import('vue-country-flag'));
  Vue.component('cForm', () => import('@/components/form/CForm'));
  Vue.component('InfiniteScrollList', () =>
    import('@/components/utils/InfiniteScrollList')
  );
  Vue.component('Container', () => import('@/components/utils/Container'));
  Vue.component('cActionButton', () =>
    import('@/components/buttons/cActionButton')
  );
  Vue.component('cDialog', () => import('@/components/dialogs/cDialog'));
  Vue.component('Date', () => import('@/components/utils/Date.vue'));
  Vue.component('vue-avatar', () => import('vue-avatar'));
};
