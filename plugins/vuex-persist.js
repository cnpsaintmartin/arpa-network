import VuexPersistence from 'vuex-persist';

export default ({ store }) => {
  window.onNuxtReady(() => {
    new VuexPersistence({
      storage: window.localStorage,
      reducer: state => ({
        informationUsageValidate: state.informationUsageValidate
      })
    }).plugin(store);
  });
};
