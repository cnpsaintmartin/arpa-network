import Vue from 'vue';
import _ from 'lodash';

const getAutoParams = res => {
  // Open API error
  if (_.get(res, 'errors')) {
    return {
      type: 'warning',
      title: 'Open API form validation',
      message: res.errors.reduce((a, e) => {
        return (a += `<strong>${e.path}:</strong> ${e.message} in ${
          e.location
        }<br><br>`);
      }, ''),
      dangerouslyUseHTMLString: true
    };
  } else if (_.get(res, 'codeError')) {
    const getTypeByCodeError = code => {
      const errorType = code.toString().charAt(1);
      switch (errorType) {
        case '1':
          return 'success';
        case '2':
          return 'info';
        case '3':
          return 'warning';
        case '4':
          return 'error';
      }
    };
    return {
      type: getTypeByCodeError(res.codeError),
      title: `Message`,
      // message: `code: ${res.codeError}`
      message: res.message
    };
  } else if (_.get(res, 'errorCode')) {
    switch (res.errorCode) {
      case 'authentication.openapi.security':
        return {
          type: 'error',
          title: 'Unauthorized',
          message: 'You cannot access this functionality'
        };
    }
  } else {
    return {
      title: 'An error occurred',
      message:
        'If error persist, please contact our support: <a href="mailto:contact@arpa.net">contact@arpa.net</a>',
      type: 'error',
      dangerouslyUseHTMLString: true
    };
  }
};

export default ({ store }) => {
  const Notifications = {
    install(Vue) {
      Vue.prototype.$notifications = {
        auto(res, options) {
          const autoParams = getAutoParams(res);
          const opt = {
            ...autoParams,
            ...options
          };
          store.commit('ADD_NOTIFICATION', opt);
        }
      };
    }
  };
  Vue.use(Notifications, {});
};
