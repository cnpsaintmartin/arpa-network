export default async ({ store }) => {
  try {
    await Promise.all([
      store.dispatch('countries/FETCH_COUNTRIES'),
      store.dispatch('countries/FETCH_LANGUAGES'),
      store.dispatch('categories/FETCH_CATEGORIES'),
      store.dispatch('tags/FETCH_TAGS'),
      store.dispatch('users/FETCH_ROLES'),
      store.dispatch('users/FETCH_STATES')
    ]);
  } catch (e) {
    console.log(e);
  }
};
