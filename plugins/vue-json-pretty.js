import Vue from 'vue';
import VueJsonPretty from 'vue-json-pretty';

export default () => {
  Vue.component('vue-json-pretty', VueJsonPretty);
};
