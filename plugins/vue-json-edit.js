import Vue from 'vue';
import vueJsonEditor from 'vue-json-editor';

export default () => {
  Vue.component('vue-json-editor', vueJsonEditor);
};
