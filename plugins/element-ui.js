import '../theme/index.css';
import ElementUI from 'element-ui';
import Vue from 'vue';
import locale from 'element-ui/lib/locale/lang/en';

export default app => {
  Vue.use(ElementUI, {
    locale,
    i18n: (key, value) => {
      try {
        app.i18n.t(key, value);
      } catch (e) {
        //console.log(e);
      }
    }
  });
};
