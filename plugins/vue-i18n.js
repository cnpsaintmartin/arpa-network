import Vue from 'vue';
import VueI18n from 'vue-i18n';

const en = require('../languages/en/lang');
const fr = require('../languages/fr/lang');
const ro = require('../languages/ro/lang');
const ge = require('../languages/ge/lang');
const el = require('../languages/el/lang');

Vue.use(VueI18n);

// eslint-disable-next-line no-unused-vars
export default ({ app, store }, inject) => {
  const messages = {
    en,
    fr,
    ro,
    ge,
    el
  };
  app.i18n = new VueI18n({
    locale: 'en',
    fallbackLocale: 'en',
    messages,
    silentTranslationWarn: true
  });
};
