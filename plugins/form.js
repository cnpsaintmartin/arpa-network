import Vue from 'vue';
import _ from 'lodash';

const Form = {
  install(Vue) {
    Vue.prototype.$form = {
      // Need context
      jsonData: function(ref) {
        try {
          return this.$refs[ref].getJsonData() || false;
        } catch (e) {
          console.log(e);
        }
      },
      validate: function(ref) {
        try {
          return this.$refs[ref].validate();
        } catch (e) {
          console.log(e);
        }
      },
      // Others
      formatField(data, label, value) {
        return data.map(c => ({
          label: c[label],
          value: c[value]
        }));
      },
      populate: function(form, data, custom) {
        const toForm = {
          ...data,
          ...custom
        };
        Object.keys(toForm).forEach(path => {
          if (!_.get(form, `inputs.${path}`)) return;
          form.inputs[path].value = toForm[path];
        });
      }
    };
  }
};
export default Form;

Vue.use(Form, {});
