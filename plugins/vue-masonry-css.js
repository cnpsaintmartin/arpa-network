import Vue from 'vue';
import VueMasonryCss from 'vue-masonry-css';

export default () => {
  Vue.use(VueMasonryCss);
};
