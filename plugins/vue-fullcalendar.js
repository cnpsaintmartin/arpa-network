import Vue from 'vue';
import VueFullcalendar from 'vue-fullcalendar';

export default () => {
  Vue.component('vue-fullcalendar', VueFullcalendar);
};
