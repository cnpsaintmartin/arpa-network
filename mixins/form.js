export const field = {
  props: {
    value: {
      type: [Array, Object, Number, String, Boolean] || null,
      required: false,
      default: () => null
    },
    options: {
      type: Object,
      require: false,
      default: () => {}
    }
  },
  computed: {
    // Valeur
    c_data: {
      get: function() {
        return this.value || null;
      },
      set: function(newValue) {
        this.$emit('update:value', newValue);
        this.$emit('update');
      }
    }
  },
  methods: {
    reset() {
      this.$emit('update:data', null);
    },
    getOption(data, path, defaultValue) {
      return this._.get(data, path, defaultValue);
    }
  }
};
export const form = {
  props: {
    onSuccess: {
      type: Function,
      required: false,
      default: null
    },
    onCancel: {
      type: Function,
      required: false,
      default: null
    }
  }
};
