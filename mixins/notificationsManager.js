import { mapGetters } from 'vuex';
export const watch = {
  computed: {
    ...mapGetters({
      activeNotifications: 'getActiveNotifications'
    })
  },
  watch: {
    activeNotifications: {
      handler() {
        this.activeNotifications.forEach(notification => {
          this.$notify({
            ...notification,
            position: 'bottom-right'
          });
          this.$store.commit('DISABLE_NOTIFICATION', notification);
        });
      },
      deep: true
    }
  }
};
