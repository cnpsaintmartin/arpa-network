/* eslint-disable */
import jwt from 'jsonwebtoken';

const check = (userRoles, roles) => userRoles.some(e => roles.includes(e));

export default async ({ route, store, redirect, req }) => {

  let isAuth = false;
  let roles;
  if (process.server) {
    if (req.cookies['auth._token.local'] === 'false' || !req.cookies['auth._token.local']) {
      return redirect(route.meta.redirect ? route.meta.redirect : '/login');
    }

    try {
      const token = jwt.verify(req.cookies['auth._token.local'].substring(7), 'secret');
      if (!token) return;
      isAuth = true;
      roles = token.roles;
    } catch (e) {
      console.error(e);
      return redirect(route.meta.redirect ? route.meta.redirect : '/login');
    }
  } else {
    isAuth = store.$auth.$state.loggedIn;
    roles = store.$auth.user.roles.map(u => u.slug);
  }

  if (!isAuth) return redirect('/login');

  const meta = route.meta[0];
  if (meta && meta.roles && !check(meta.roles, roles)) {
    return redirect(meta.redirect ? meta.redirect : '/');
  }
};
