// ACTIONS
const SEND_MAIL = `SEND_MAIL`;

export const actions = {
  async [SEND_MAIL](_, data) {
    try {
      const result = await this.$axios.$post(`/mail`, data);
      return result;
    } catch (e) {
      throw e;
    }
  }
};
