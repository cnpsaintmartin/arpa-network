import { getField, updateField } from 'vuex-map-fields';
// ACTIONS
const FETCH_SPONSORS = `FETCH_SPONSORS`;
const FETCH_SPONSOR = `FETCH_SPONSOR`;
const CREATE_SPONSOR = `CREATE_SPONSOR`;
const EDIT_SPONSOR = `EDIT_SPONSOR`;
const DELETE_SPONSOR = `DELETE_SPONSOR`;

export const state = () => ({
  sponsors: []
});

export const mutations = {
  updateField
};

export const getters = {
  getField
};

export const actions = {
  async [FETCH_SPONSORS](_, data) {
    try {
      const result = await this.$axios.$get(`/sponsors`, {
        params: {
          ...data
        }
      });
      return result || [];
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_SPONSOR](_, id) {
    try {
      const result = await this.$axios.$get(`/sponsor/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_SPONSOR](_, data) {
    try {
      const result = await this.$axios.$post('/sponsor', data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_SPONSOR](_, { id, data }) {
    try {
      const result = await this.$axios.$put(`/sponsor/${id}`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_SPONSOR](_, id) {
    try {
      const result = await this.$axios.$delete(`/sponsor/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  }
};
