import notifTypes from './notificationTypes';
import _ from 'lodash';

// ACTIONS
const FETCH_USER = `FETCH_USER`;
const FETCH_USER_BY_ID = `FETCH_USER_BY_ID`;
const FETCH_USERS = `FETCH_USERS`;
const SEND_CONTACT_REQUEST = `SEND_CONTACT_REQUEST`;
const REGISTER = `REGISTER`;
const FORGOT_PASSWORD = `FORGOT_PASSWORD`;
const RESET_PASSWORD = `RESET_PASSWORD`;
const EDIT_DATA = `EDIT_DATA`;
const FETCH_ROLES = `FETCH_ROLES`;
const FETCH_STATES = `FETCH_STATES`;
const SET_ROLES = `SET_ROLES`;
const SET_STATES = `SET_STATES`;
const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';
const RESPOND_TO_CONTACT_REQUEST = 'RESPOND_TO_CONTACT_REQUEST';
const SEND_COMMITTEE_REQUEST = `SEND_COMMITTEE_REQUEST`;
const RESPOND_TO_COMMITTEE_REQUEST = `RESPOND_TO_COMMITTEE_REQUEST`;
const RESPOND_TO_PROFESSIONAL_REQUEST = `RESPOND_TO_PROFESSIONAL_REQUEST`;
const FETCH_NOTIFICATIONS_COUNTER = `FETCH_NOTIFICATIONS_COUNTER`;
const CREATE_GDPR_FILE = `CREATE_GDPR_FILE`;
const DELETE_GDPR = `DELETE_GDPR`;
const SEND_FEEDBACK = `SEND_FEEDBACK`;
const FETCH_FEEDBACKS = `FETCH_FEEDBACKS`;
const FETCH_FEEDBACK = `FETCH_FEEDBACK`;
const FETCH_STATISTICS = `FETCH_STATISTICS`;
const LEAVE_COMMITTEE = `LEAVE_COMMITTEE`;

export const state = () => ({
  roles: [],
  states: [],
  notificationTypes: notifTypes
});

export const getters = {
  roles: state => state.roles,
  // en attente, état confirmé
  states: state => state.states,
  notificationTypes: state => state.notificationTypes,
  getRole: state => slug => state.roles.find(r => r.slug === slug),
  getState: state => slug => state.states.find(s => s.slug === slug)
};

export const mutations = {
  [SET_ROLES](state, roles) {
    state.roles = roles;
  },
  [SET_STATES](state, states) {
    state.states = states;
  }
};

export const actions = {
  async [FETCH_USER]() {
    try {
      const result = await this.$axios.$get(`/user`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_USER_BY_ID](_, id) {
    try {
      const result = await this.$axios.$get(`/user/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  // Authentification
  async [REGISTER](_, data) {
    try {
      const result = await this.$axios.$post(`/user`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FORGOT_PASSWORD](_, data) {
    try {
      return await this.$axios.$post('/user/forgotPassword', data);
    } catch (e) {
      throw e;
    }
  },
  async [RESET_PASSWORD](_, data) {
    try {
      return await this.$axios.$post('/user/resetPassword', data);
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_DATA](_, { data, id }) {
    try {
      const result = await this.$axios.$put(`/user/${id}`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_USERS](_, data) {
    try {
      const result = await this.$axios.$get(`/users`, {
        params: {
          ...data
        }
      });
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_ROLES]({ commit }) {
    try {
      const result = await this.$axios.$get(`/roles`);
      commit(SET_ROLES, result.docs);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_STATES]({ commit }) {
    commit(SET_STATES, [
      {
        _id: 'dsvcfd786',
        name: 'banned',
        slug: 'banned'
      },
      {
        _id: 'ds678vcfd786',
        name: 'Committee request',
        slug: 'committee_request'
      }
    ]);
    /*try {
      const result = await this.$axios.$get(`/states`);
      // state.states = result.docs;
      return result;
    } catch (e) {
      throw e;
    }*/
  },
  async [SEND_CONTACT_REQUEST](_, { id, data }) {
    try {
      const result = await this.$axios.$post(`/contactRequest/${id}`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [RESPOND_TO_CONTACT_REQUEST](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/notification/${id}/contactRequest`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_NOTIFICATION](_, id) {
    try {
      const result = await this.$axios.$delete(`/notification/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [SEND_COMMITTEE_REQUEST](_, data) {
    try {
      const result = await this.$axios.$post('/committeeRequest', data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [RESPOND_TO_COMMITTEE_REQUEST](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/committeeRequest/${id}/validation`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [RESPOND_TO_PROFESSIONAL_REQUEST](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/professionalRequest/${id}/validation`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_NOTIFICATIONS_COUNTER]() {
    try {
      const result = await this.$axios.$get(`/user/numberOfMyNotifications`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_GDPR_FILE]({ rootState }) {
    const id = _.get(rootState, 'auth.user._id');
    try {
      const result = await this.$axios.$get(`/user/${id}/gdpr`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_GDPR]({ rootState }) {
    const id = _.get(rootState, 'auth.user._id');
    try {
      const result = await this.$axios.$delete(`/user/${id}/gdpr`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [SEND_FEEDBACK](_, data) {
    try {
      const result = await this.$axios.$post(`/satisfactionSurvey`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_FEEDBACKS](_, data) {
    try {
      const result = await this.$axios.$get(`/committee/satisfactionSurveys`, {
        params: {
          ...data
        }
      });
      return result.docs;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_FEEDBACK](_, id) {
    try {
      const result = await this.$axios.$get(
        `/committee/satisfactionSurvey/${id}`
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_STATISTICS]() {
    try {
      const result = await this.$axios.$get(`/committee/stats`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [LEAVE_COMMITTEE](_, { id }) {
    try {
      const result = await this.$axios.$put(`/user/${id}/leaveCommittee`);
      return result;
    } catch (e) {
      throw e;
    }
  }
};
