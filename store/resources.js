import { getField, updateField } from 'vuex-map-fields';
import _ from 'lodash';
// MUTATIONS
const SET_RESOURCES = `SET_RESOURCES`;
const PUSH_RESOURCES = `PUSH_RESOURCES`;
// ACTIONS
const FETCH_RESOURCES = `FETCH_RESOURCES`;
const FETCH_COMMITTEE_RESOURCES = `FETCH_COMMITTEE_RESOURCES`;
const FETCH_COMMITTEE_COMMENTS = `FETCH_COMMITTEE_COMMENTS`;
const FETCH_USER_RESOURCES = `FETCH_USER_RESOURCES`;
const FETCH_RESOURCE = `FETCH_RESOURCE`;
const CREATE_RESOURCE = `CREATE_RESOURCE`;
const EDIT_RESOURCE = `EDIT_RESOURCE`;
const DELETE_RESOURCE = `DELETE_RESOURCE`;
const VALIDATE_RESOURCE = `VALIDATE_RESOURCE`;
const CREATE_COMMENT = `CREATE_COMMENT`;
const FETCH_TAGS = `FETCH_TAGS`;
const ADD_LIKE = `ADD_LIKE`;
const DELETE_LIKE = `DELETE_LIKE`;
const ADD_ANSWER = `ADD_ANSWER`;
const REPORT_RESOURCE = `REPORT_RESOURCE`;
const REPORT_RESOURCE_COMMENT = `REPORT_RESOURCE_COMMENT`;
const APPROVE_REJECT_REPORT_RESOURCE_COMMENT = `APPROVE_REJECT_REPORT_RESOURCE_COMMENT`;
const APPROVE_REJECT_REPORT_RESOURCE = `APPROVE_REJECT_REPORT_RESOURCE`;
const DELETE_RESOURCE_COMMENT = `DELETE_RESOURCE_COMMENT`;
const COUNT_RESOURCES = `COUNT_RESOURCES`;
const COUNT_REPORTS = `COUNT_REPORTS`;

export const state = () => ({
  resources: [],
  states: [
    {
      name: 'Validating',
      resourceStatus: 1,
      type: 'warning'
    },
    {
      name: 'Rejected',
      resourceStatus: 2,
      type: 'danger'
    },
    {
      name: 'Published',
      resourceStatus: 3,
      type: 'success'
    },
    {
      name: 'Edit validating',
      resourceStatus: 4,
      type: 'warning'
    },
    {
      name: 'Edit rejected',
      resourceStatus: 5,
      type: 'danger'
    }
  ]
});

export const mutations = {
  [SET_RESOURCES](state, payload) {
    state.resources = payload;
  },
  [PUSH_RESOURCES](state, payload) {
    state.resources = [...state.resources, ...payload];
  },
  updateField
};

export const getters = {
  resources: state => state.resources,
  getState: state => resourceStatus =>
    state.states.find(s => s.resourceStatus === resourceStatus),
  getField
};

export const actions = {
  async [FETCH_RESOURCES](_, data) {
    try {
      const result = await this.$axios.$get('/resources', {
        params: {
          ...data
        }
      });
      const reset = data.reset || false;
      if (!reset && !result.docs)
        return {
          docs: []
        };
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_COMMITTEE_RESOURCES](_, data) {
    try {
      const result = await this.$axios.$get(`/committee/resources`, {
        params: {
          ...data
        }
      });
      return result || [];
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_COMMITTEE_COMMENTS](_, data) {
    try {
      const result = await this.$axios.$get(`/committee/comments`, {
        params: {
          ...data
        }
      });
      return result || [];
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_USER_RESOURCES]({ rootState }, data) {
    const id = _.get(rootState, 'auth.user._id');
    try {
      const result = await this.$axios.$get(`/user/${id}/resources`, {
        params: {
          ...data
        }
      });
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_RESOURCE](_, id) {
    try {
      const result = await this.$axios.$get(
        `${process.server ? '/api' : ''}/resource/${id}`
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_RESOURCE](_, data) {
    try {
      const result = await this.$axios.$post('/resource', data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_RESOURCE](_, { id, data }) {
    try {
      const result = await this.$axios.$put(`/resource/${id}`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_RESOURCE](_, id) {
    try {
      const result = await this.$axios.$delete(`/resource/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [VALIDATE_RESOURCE](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/resource/${id}/validation`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_COMMENT](_, { id, text }) {
    try {
      const result = await this.$axios.$post(`/resource/${id}/comment`, {
        text
      });
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_TAGS](_, data) {
    try {
      const result = await this.$axios.$get('/tags', {
        params: {
          ...data
        }
      });
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [ADD_LIKE](_, id) {
    try {
      const result = await this.$axios.$post(`/resource/${id}/like`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_LIKE](_, id) {
    try {
      const result = await this.$axios.$delete(`/resource/${id}/like`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [ADD_ANSWER](_, { idResource, idAnswer }) {
    try {
      const result = await this.$axios.$post(
        `/resource/${idResource}/answer/${idAnswer}`
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [REPORT_RESOURCE](_, { id, data }) {
    try {
      const result = await this.$axios.$post(`/resource/${id}/report`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [REPORT_RESOURCE_COMMENT](_, { id, data }) {
    try {
      const result = await this.$axios.$post(`/comment/${id}/report`, data);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [APPROVE_REJECT_REPORT_RESOURCE_COMMENT](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/comment/${id}/validationReport`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [APPROVE_REJECT_REPORT_RESOURCE](_, { id, data }) {
    try {
      const result = await this.$axios.$post(
        `/resource/${id}/validationReport`,
        data
      );
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_RESOURCE_COMMENT](_, id) {
    try {
      const result = await this.$axios.$delete(`/comment/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [COUNT_RESOURCES](_, data) {
    try {
      const result = await this.$axios.$get('/committee/numberResources', {
        params: data
      });
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [COUNT_REPORTS](_, data) {
    try {
      const result = await this.$axios.$get('/committee/numberReport', {
        params: data
      });
      return result;
    } catch (e) {
      throw e;
    }
  }
};
