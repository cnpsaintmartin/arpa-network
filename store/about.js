import { getField, updateField } from 'vuex-map-fields';
// MUTATIONS
const SET_ARPA_PROJECT = `SET_ARPA_PROJECT`;
// ACTIONS
const FETCH_MEMBERS = `FETCH_MEMBERS`;
const FETCH_MEMBER = `FETCH_MEMBER`;
const CREATE_MEMBER = `CREATE_MEMBER`;
const EDIT_MEMBER = `EDIT_MEMBER`;
const DELETE_MEMBER = `DELETE_MEMBER`;
const FETCH_REPORTS = `FETCH_REPORTS`;
const FETCH_REPORT = `FETCH_REPORT`;
const CREATE_REPORT = `CREATE_REPORT`;
const EDIT_REPORT = `EDIT_REPORT`;
const DELETE_REPORT = `DELETE_REPORT`;
const FETCH_ARPA_PROJECT = `FETCH_ARPA_PROJECT`;
const EDIT_ARPA_PROJECT = `EDIT_ARPA_PROJECT`;

export const state = () => ({
  sponsors: [],
  arpaProject: {}
});

export const mutations = {
  [SET_ARPA_PROJECT](state, payload) {
    state.arpaProject = payload;
  },
  updateField
};

export const getters = {
  arpaProject: state => state.arpaProject,
  getField
};

export const actions = {
  async [FETCH_MEMBERS](_, data) {
    try {
      const result = await this.$axios.$get(`/projectMembers`, {
        params: {
          ...data
        }
      });
      return result || [];
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_MEMBER](_, id) {
    try {
      const result = await this.$axios.$get(`/projectMember/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_MEMBER]({ dispatch }, data) {
    try {
      const result = await this.$axios.$post('/projectMember', data);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_MEMBER]({ dispatch }, { id, data }) {
    try {
      const result = await this.$axios.$put(`/projectMember/${id}`, data);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_MEMBER]({ dispatch }, id) {
    try {
      const result = await this.$axios.$delete(`/projectMember/${id}`);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_REPORTS](_, data) {
    try {
      const result = await this.$axios.$get(`/projectReports`, {
        params: {
          ...data
        }
      });
      return result || [];
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_REPORT](_, id) {
    try {
      const result = await this.$axios.$get(`/projectReport/${id}`);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [CREATE_REPORT]({ dispatch }, data) {
    try {
      const result = await this.$axios.$post('/projectReport', data);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_REPORT]({ dispatch }, { id, data }) {
    try {
      const result = await this.$axios.$put(`/projectReport/${id}`, data);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [DELETE_REPORT]({ dispatch }, id) {
    try {
      const result = await this.$axios.$delete(`/projectReport/${id}`);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_ARPA_PROJECT]({ commit }) {
    try {
      const result = await this.$axios.$get('/arpaProject');
      commit(SET_ARPA_PROJECT, result.docs);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_ARPA_PROJECT]({ dispatch }, data) {
    try {
      const result = await this.$axios.$put(`/arpaProject`, data);
      dispatch(FETCH_ARPA_PROJECT);
      return result;
    } catch (e) {
      throw e;
    }
  }
};
