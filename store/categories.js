import { getField, updateField } from 'vuex-map-fields';

// MUTATIONS
const SET_CATEGORIES = `SET_CATEGORIES`;
// ACTIONS
const FETCH_CATEGORIES = `FETCH_CATEGORIES`;

export const state = () => ({
  categories: []
});

export const mutations = {
  [SET_CATEGORIES](state, payload) {
    state.categories = payload;
  },
  updateField
};

export const getters = {
  categories: state => state.categories,
  getField
};

export const actions = {
  async [FETCH_CATEGORIES]({ commit }) {
    try {
      const { docs } = await this.$axios.$get('/categories', {
        params: {
          page: 1,
          limit: 10
        }
      });
      commit(SET_CATEGORIES, docs);
    } catch (e) {
      throw e;
    }
  }
};
