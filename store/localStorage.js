import { getField, updateField } from 'vuex-map-fields';

export const state = () => ({});

export const mutations = {
  updateField
};

export const getters = {
  getField
};
