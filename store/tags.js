import { getField, updateField } from 'vuex-map-fields';

// MUTATIONS
const SET_TAGS = `SET_TAGS`;
// ACTIONS
const FETCH_TAGS = `FETCH_TAGS`;

export const state = () => ({
  tags: []
});

export const mutations = {
  [SET_TAGS](state, payload) {
    state.tags = payload;
  },
  updateField
};

export const getters = {
  tags: state => state.tags,
  getField
};

export const actions = {
  async [FETCH_TAGS]({ commit }) {
    try {
      const { docs } = await this.$axios.$get('/tags', {
        params: {
          page: 1,
          limit: 10
        }
      });
      commit(SET_TAGS, docs);
    } catch (e) {
      throw e;
    }
  }
};
