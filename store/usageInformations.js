// MUTATIONS
const SET_TERMS_OF_SERVICE = `SET_TERMS_OF_SERVICE`;
const SET_LEGALS_NOTICE = `SET_LEGALS_NOTICE`;
const SET_AUTHOR_CODE_CONDUCT = `SET_AUTHOR_CODE_CONDUCT`;
const SET_COMMITTEE_CODE_CONDUCT = `SET_COMMITTEE_CODE_CONDUCT`;
// ACTIONS
const FETCH_TERMS_OF_SERVICE = `FETCH_TERMS_OF_SERVICE`;
const FETCH_LEGALS_NOTICE = `FETCH_LEGALS_NOTICE`;
const EDIT_TERMS_OF_SERVICE = `EDIT_TERMS_OF_SERVICE`;
const EDIT_LEGALS_NOTICE = `EDIT_LEGALS_NOTICE`;
const FETCH_AUTHOR_CODE_CONDUCT = `FETCH_AUTHOR_CODE_CONDUCT`;
const EDIT_AUTHOR_CODE_CONDUCT = `EDIT_AUTHOR_CODE_CONDUCT`;
const FETCH_COMMITTEE_CODE_CONDUCT = `FETCH_COMMITTEE_CODE_CONDUCT`;
const EDIT_COMMITTEE_CODE_CONDUCT = `EDIT_COMMITTEE_CODE_CONDUCT`;

export const state = () => ({
  termsOfService: {
    content: ''
  },
  legalsNotice: {
    content: ''
  },
  authorCodeConduct: {
    content: ''
  },
  committeeCodeConduct: {
    content: ''
  }
});

export const mutations = {
  [SET_TERMS_OF_SERVICE](state, payload) {
    state.termsOfService = payload;
  },
  [SET_LEGALS_NOTICE](state, payload) {
    state.legalsNotice = payload;
  },
  [SET_AUTHOR_CODE_CONDUCT](state, payload) {
    state.authorCodeConduct = payload;
  },
  [SET_COMMITTEE_CODE_CONDUCT](state, payload) {
    state.committeeCodeConduct = payload;
  }
};

export const getters = {
  termsOfService: state => state.termsOfService,
  legalsNotice: state => state.legalsNotice,
  authorCodeConduct: state => state.authorCodeConduct,
  committeeCodeConduct: state => state.committeeCodeConduct
};

export const actions = {
  async [FETCH_TERMS_OF_SERVICE]({ commit }) {
    try {
      const result = await this.$axios.$get(
        `${process.server ? '/api' : ''}/termsOfService`
      );
      if (!result) return;
      commit(SET_TERMS_OF_SERVICE, result.docs);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_LEGALS_NOTICE]({ commit }) {
    try {
      const result = await this.$axios.$get(
        `${process.server ? '/api' : ''}/legalsNotice`
      );
      if (!result) return;
      commit(SET_LEGALS_NOTICE, result.docs);
      return result;
    } catch (e) {
      console.log(e);
      throw e;
    }
  },
  async [EDIT_TERMS_OF_SERVICE]({ dispatch }, data) {
    try {
      const result = await this.$axios.$put(`/termsOfService`, data);
      dispatch(FETCH_TERMS_OF_SERVICE);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_LEGALS_NOTICE]({ dispatch }, data) {
    try {
      const result = await this.$axios.$put(`/legalsNotice`, data);
      dispatch(FETCH_LEGALS_NOTICE);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [EDIT_AUTHOR_CODE_CONDUCT]({ dispatch }, data) {
    try {
      const result = await this.$axios.$put(`/authorCodeConduct`, data);
      dispatch(FETCH_AUTHOR_CODE_CONDUCT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_AUTHOR_CODE_CONDUCT]({ commit }) {
    try {
      const result = await this.$axios.$get(
        `${process.server ? '/api' : ''}/authorCodeConduct`
      );
      if (!result) return;
      commit(SET_AUTHOR_CODE_CONDUCT, result.docs);
      return result;
    } catch (e) {
      console.log(e);
      throw e;
    }
  },
  async [EDIT_COMMITTEE_CODE_CONDUCT]({ dispatch }, data) {
    try {
      const result = await this.$axios.$put(`/committeeCodeConduct`, data);
      dispatch(FETCH_COMMITTEE_CODE_CONDUCT);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_COMMITTEE_CODE_CONDUCT]({ commit }) {
    try {
      const result = await this.$axios.$get(
        `${process.server ? '/api' : ''}/committeeCodeConduct`
      );
      if (!result) return;
      commit(SET_COMMITTEE_CODE_CONDUCT, result.docs);
      return result;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }
};
