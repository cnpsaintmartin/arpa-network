import { getField, updateField } from 'vuex-map-fields';

const version = require('../package.json').version;
// Mutations
const ADD_NOTIFICATION = `ADD_NOTIFICATION`;
const DISABLE_NOTIFICATION = `DISABLE_NOTIFICATION`;

export const state = () => ({
  version,
  informationUsageValidate: false,
  projectDescription: false,
  tutorial: false,
  notifications: [],
  feedbackDialog: false
});

export const mutations = {
  [ADD_NOTIFICATION](state, notification) {
    state.notifications.push({
      ...notification,
      displayed: false
    });
  },
  [DISABLE_NOTIFICATION](state, notification) {
    notification.displayed = true;
  },
  updateField
};

export const getters = {
  version: state => state.version,
  getActiveNotifications: state =>
    state.notifications.filter(n => !n.displayed),
  getField
};

export const actions = {
  async nuxtServerInit() {}
};
