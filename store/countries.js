// MUTATIONS
const SET_COUNTRIES = `SET_COUNTRIES`;
const SET_LANGUAGES = `SET_LANGUAGES`;
// ACTIONS
const FETCH_COUNTRIES = `FETCH_COUNTRIES`;
const FETCH_LANGUAGES = `FETCH_LANGUAGES`;

export const state = () => ({
  countries: [],
  languages: []
});

export const getters = {
  countries: state => state.countries,
  languages: state => state.languages
};

export const mutations = {
  [SET_COUNTRIES](state, payload) {
    state.countries = payload;
  },
  [SET_LANGUAGES](state, payload) {
    state.languages = payload;
  }
};

export const actions = {
  async [FETCH_COUNTRIES]({ commit }) {
    try {
      const result = await this.$axios.$get(`/countries`);
      commit(SET_COUNTRIES, result.docs);
      return result;
    } catch (e) {
      throw e;
    }
  },
  async [FETCH_LANGUAGES]({ commit }) {
    try {
      const result = await this.$axios.$get(`/languages`);
      commit(SET_LANGUAGES, result.docs);
      return result;
    } catch (e) {
      throw e;
    }
  }
};
