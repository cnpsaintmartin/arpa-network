const notifications = [
  {
    type: 'contactRequest',
    mode: 'avatar',
    displaySenderName: true,
    color: 'primary',
    actions: [
      {
        label: 'Reject',
        type: 'default',
        method: 'contactRequestRejected'
      },
      {
        label: 'Accept',
        type: 'primary',
        method: 'contactRequestAccepted'
      }
    ]
  },
  {
    type: 'contactRequestRefused',
    mode: 'avatar',
    color: 'danger'
  },
  {
    type: 'contactRequestAccepted',
    mode: 'avatar',
    displaySenderName: true,
    color: 'success'
  },
  {
    type: 'resourcePublished',
    mode: 'icon',
    color: 'success',
    icon: 'check'
  },
  {
    type: 'resourcePublishRequest',
    mode: 'icon',
    color: 'warning',
    icon: 'newspaper',
    actions: [
      {
        label: 'View',
        type: 'primary',
        method: 'resourcePublishRequestView'
      }
    ]
  },
  {
    type: 'resourceRejected',
    mode: 'icon',
    color: 'danger',
    icon: 'times'
  },
  {
    type: 'resourceReported',
    mode: 'icon',
    color: 'warning',
    icon: 'newspaper',
    actions: [
      {
        label: 'View',
        type: 'primary',
        method: 'resourceReportedView'
      }
    ]
  },
  {
    type: 'resourceDeleted',
    mode: 'icon',
    color: 'danger',
    icon: 'newspaper'
  },
  {
    type: 'commentReported',
    mode: 'icon',
    color: 'warning',
    icon: 'comments',
    actions: [
      {
        label: 'View',
        type: 'primary',
        method: 'commentReportedView'
      }
    ]
  },
  {
    type: 'commentDeleted',
    mode: 'icon',
    color: 'danger',
    icon: 'comments'
  },
  {
    type: 'reportAccepted',
    mode: 'icon',
    color: 'success',
    icon: 'flag'
  },
  {
    type: 'reportRejected',
    mode: 'icon',
    color: 'warning',
    icon: 'flag'
  },
  {
    type: 'committeeMemberRequest',
    mode: 'avatar',
    displaySenderName: true,
    color: 'primary',
    actions: [
      {
        label: 'No',
        type: 'default',
        method: 'committeeMemberRequestRejected'
      },
      {
        label: 'Yes',
        type: 'primary',
        method: 'committeeMemberRequestAccepted'
      }
    ]
  },
  {
    type: 'committeeMemberAccepted',
    mode: 'icon',
    color: 'success',
    icon: 'users'
  },
  {
    type: 'committeeMemberRefused',
    mode: 'icon',
    color: 'warning',
    icon: 'users'
  },
  {
    type: 'professionalMemberRequest',
    mode: 'avatar',
    color: 'primary',
    displaySenderName: true,
    actions: [
      {
        label: 'View',
        type: 'primary',
        method: 'professionalMemberRequestView'
      }
    ]
  },
  {
    type: 'professionalMemberAccepted',
    mode: 'icon',
    color: 'success',
    icon: 'users'
  },
  {
    type: 'professionalMemberRefused',
    mode: 'icon',
    color: 'danger',
    icon: 'users'
  },
  {
    type: 'userCommentBanned',
    mode: 'icon',
    color: 'danger',
    icon: 'times'
  }
];
export default notifications;
