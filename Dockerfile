FROM node:10-alpine
LABEL MAINTAINER="jdieuze"
LABEL version="1.0"

RUN mkdir -p /app
COPY . /app
WORKDIR /app

ENV HOST 0.0.0.0

RUN apk add --no-cache python py-pip git
RUN npm cache clean --force
RUN npm install -q
RUN npm rb
RUN echo "0.0.0.0 arpa-ageing.eu" >> /etc/hosts
RUN npm run build
RUN apk del python py-pip git


EXPOSE 3000/tcp

ENTRYPOINT ["npm", "start"]
